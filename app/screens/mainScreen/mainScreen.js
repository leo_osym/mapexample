import React, { Fragment, useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';

const MainScreen = () => {
    const [markers, setMarkers] = useState([{ coordinate: { longitude: -122.4350943043828, latitude: 37.79016290297611 }, title: '', description: '' }]);

    setNewMarkers = (coordinate, title, description) => {
        var mark = [...markers]
        mark.push({ coordinate, title, description });
        setMarkers(mark);
        console.log(markers);
    }

    deleteMarker = (index)=>{
        var mark = [...markers];
        mark.splice(index, 1);
        setMarkers(mark);
    }

    useEffect(() => {
    });

    return (
        <Fragment>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
                <View style={styles.container}>
                    <Text>Hello</Text>
                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        //onLongPress={e => console.log(e.nativeEvent)}
                        onLongPress={e => setNewMarkers(e.nativeEvent.coordinate,
                            '', '')}
                        region={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }} >
                        {
                            markers.map((marker, index) => (
                                <Marker
                                    key={index}
                                    onPress={e => {deleteMarker(index)}}
                                    coordinate={marker.coordinate}
                                    title={marker.title}
                                    description={marker.description}
                                />
                            ))
                        }
                    </MapView>
                </View>
            </SafeAreaView>
        </Fragment>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    },
    text: {
        color: 'red',
        fontSize: 30
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});

export default MainScreen;
